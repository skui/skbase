/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef __SKBASE_TYPES_ARRAY_H__
#define __SKBASE_TYPES_ARRAY_H__


#include "skbase/api.h"
#include "skbase/types/array_def.h"
#include "skbase/types/size_def.h"

#include <stdlib.h>
#include <string.h>


SKBASE_API_CALL skbase_array*
skbase_array_create(skbase_array* array,
                  skbase_size element_size)
{
  array->start = NULL;
  array->end = NULL;

  array->length = 0;
  array->element_size = element_size;

  return array;
}


SKBASE_API_CALL void*
skbase_array_copy(skbase_array* dest,
                skbase_array* source)
{
  dest->start = malloc(source->end - source->start);
  dest->end = dest->start + (source->end - source->start);

  if (!dest->start)
    return NULL;

  memcpy(dest->start, source->start, source->length * source->element_size);
  dest->length = source->length;
  dest->element_size = source->element_size;

  return dest->start;
}


SKBASE_API_CALL void
skbase_array_free(skbase_array* array)
{
  free(array->start);

  array->end = array->start;
  array->length = 0;
}


SKBASE_API_CALL void*
skbase_array_realloc(skbase_array* array,
                   skbase_size length)
{
  array->start = realloc(array->start, length * array->element_size);
  array->end = array->start + (length * array->element_size);
  array->length = length;

  return array->start;
}


SKBASE_API_CALL void*
skbase_array_get(skbase_array* array,
               skbase_size id)
{
  if (id >= array->length)
    return NULL;

  return array->start + (id * array->element_size);
}


SKBASE_API_CALL void*
skbase_array_get_value(skbase_array* array,
                     skbase_size id,
                     void* value)
{
  void* ptr = skbase_array_get(array, id);

  if (!ptr)
    return NULL;

  if (!value)
    return NULL;

  memcpy(value, ptr, array->element_size);

  return ptr;
}


SKBASE_API_CALL void*
skbase_array_set(skbase_array* array,
               skbase_size id,
               void* element)
{
  if (id >= array->length)
    return NULL;

  if (element)
    return memcpy(array->start + (id * array->element_size),
                  element,
                  array->element_size);
  else
    return memset(array->start + (id * array->element_size),
                  0,
                  array->element_size);
}


SKBASE_API_CALL void*
skbase_array_push(skbase_array* array,
                void* element)
{
  if (!skbase_array_realloc(array, array->length + 1))
    return NULL;

  return skbase_array_set(array, array->length - 1, element);
}


SKBASE_API_CALL void*
skbase_array_pop(skbase_array* array)
{
  if (array->length == 0)
    return NULL;

  if (!skbase_array_realloc(array, array->length - 1))
    return NULL;

  return array->start;
}


SKBASE_API_CALL void*
skbase_array_memmove(skbase_array* array,
                   skbase_size source,
                   skbase_size dest,
                   skbase_size length)
{
  return memmove(array->start + (dest * array->element_size),
                 array->start + (source * array->element_size),
                 (length - source) * array->element_size);
}


SKBASE_API_CALL void*
skbase_array_insert(skbase_array* array,
                  skbase_size id,
                  void* element)
{
  if (id > array->length)
    return NULL;

  if (id == array->length)
    return skbase_array_push(array, element);

  if (!skbase_array_realloc(array, array->length + 1))
    return NULL;

  skbase_array_memmove(array, id, id + 1, array->length - 1);
  return skbase_array_set(array, id, element);
}


SKBASE_API_CALL void*
skbase_array_remove(skbase_array* array,
                  skbase_size id)
{
  if (id > array->length)
    return NULL;

  if (id == array->length)
    return skbase_array_pop(array);

  skbase_array_memmove(array, id + 1, id, array->length);
  return skbase_array_realloc(array, array->length - 1);
}


SKBASE_API_CALL skbase_ssize
skbase_array_indexof(skbase_array* array,
                   void* element)
{
  skbase_ssize i;

  for (i = 0; i < (skbase_ssize)array->length; i++)
    {
      if (!memcmp(array + (i * array->element_size), element,
                  array->element_size))
        return i;
    }

  return -1;
}


#endif
