/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef __SKBASE_TYPES_MAP_H__
#define __SKBASE_TYPES_MAP_H__


#include "skbase/api.h"
#include "skbase/types/map_def.h"
#include "skbase/types/array.h"
#include "skbase/types/string.h"

#include <stdlib.h>
#include <string.h>


#define skbase_map_copy skbase_array_copy
#define skbase_map_free skbase_array_free


SKBASE_API_CALL skbase_map*
skbase_map_create(skbase_map* map)
{
  return skbase_array_create(map, sizeof(skbase_map_kv));
}


SKBASE_API_CALL skbase_ssize
skbase_map_indexof(skbase_map* map,
                 skbase_string key)
{
  skbase_map_kv* map_iter = map->start;

  for (map_iter = (skbase_map_kv*)map->start;
       map_iter < (skbase_map_kv*)map->end;
       map_iter++)
    {
      if (skbase_string_eq(map_iter->key, key))
        return map_iter - (skbase_map_kv*)map->start;
    }

  return -1;
}


SKBASE_API_CALL skbase_map_kv*
skbase_map_get_kv(skbase_map* map,
                skbase_string key)
{
  skbase_ssize index = skbase_map_indexof(map, key);

  if (index < 0)
    return NULL;

  return skbase_array_get(map, index);
}


SKBASE_API_CALL void*
skbase_map_get(skbase_map* map,
             skbase_string key)
{
  skbase_map_kv* kv = skbase_map_get_kv(map, key);

  if (!kv)
    return NULL;

  return kv->value;
}


SKBASE_API_CALL void*
skbase_map_set(skbase_map* map,
             skbase_string key,
             void* value)
{
  skbase_map_kv* kv = skbase_map_get_kv(map, key);
  skbase_map_kv localkv;

  if (!kv)
    {
      localkv.key = key;
      localkv.value = value;

      return skbase_array_push(map, &localkv);
    }
  else
    {
      kv->value = value;

      return kv;
    }
}


#endif
