/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef __SKBASE_TEST_CHECK_H__
#define __SKBASE_TEST_CHECK_H__


#include "skbase/types/string.h"
#include <stdio.h>


#define SKBASE_TEST_ASSERT(x) {                                         \
    if (!(x))                                                           \
      {                                                                 \
        fprintf(stderr, "FAIL: %s (%s:%d)\n", #x, __FILE__, __LINE__);  \
        return 0;                                                       \
      }                                                                 \
  }

#define SKBASE_TEST_EQd(x, y) {                                 \
    if ((x) != (y))                                             \
      {                                                         \
        fprintf(stderr, "FAIL: %d != %d (%s:%d)\n", x, y,       \
                __FILE__, __LINE__);                            \
        return 0;                                               \
      }                                                         \
  }

#define SKBASE_TEST_EQlud(x, y) {                               \
    if ((x) != (y))                                             \
      {                                                         \
        fprintf(stderr, "FAIL: %lud != %lud (%s:%d)\n", x, y,   \
                __FILE__, __LINE__);                            \
        return 0;                                               \
      }                                                         \
  }

#define SKBASE_TEST_STREQ(x, y) {                               \
    if (!skbase_string_eq((x), (y)))                            \
      {                                                         \
        fprintf(stderr, "FAIL: %s != %s (%s:%d)\n", x, y,       \
                __FILE__, __LINE__);                            \
        return 0;                                               \
      }                                                         \
  }


#endif
