/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef __SKBASE_TEST_MAIN_H__
#define __SKBASE_TEST_MAIN_H__


#include "skbase/test/case.h"
#include <stdio.h>
#include <stdlib.h>


SKBASE_TESTS;


int
main(int argc, char** argv)
{
  int i = 0;

  skbase_test_case* test = skbase_tests;

  if (argc > 1)
    i = atoi(argv[1]);

  for (; test->name; i++, test++)
    {
      if (!test->fcn())
        {
          fprintf(stderr, "Test %s (%i) failed\n", test->name, i);
          return i + 1;
        }
    }

  return 0;
}


#endif
