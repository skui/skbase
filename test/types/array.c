/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#include "skbase/test/test.h"
#include "skbase/types/array.h"


int
skbase_test_array_copy(void)
{
  skbase_array array;
  skbase_array array2;
  int x[4] = {0, 1, 2, 3};
  int y[4];

  skbase_array_create(&array, sizeof(int));

  skbase_array_push(&array, x + 0);
  skbase_array_push(&array, x + 1);
  skbase_array_push(&array, x + 2);
  skbase_array_push(&array, x + 3);

  skbase_array_copy(&array2, &array);

  skbase_array_get_value(&array2, 0, y + 0);
  skbase_array_get_value(&array2, 1, y + 1);
  skbase_array_get_value(&array2, 2, y + 2);
  skbase_array_get_value(&array2, 3, y + 3);

  SKBASE_TEST_EQd(x[0], y[0]);
  SKBASE_TEST_EQd(x[1], y[1]);
  SKBASE_TEST_EQd(x[2], y[2]);
  SKBASE_TEST_EQd(x[3], y[3]);


  skbase_array_free(&array);
  skbase_array_free(&array2);


  return 1;
}


int
skbase_test_array_pushpop(void)
{
  skbase_array array;
  int x[4] = {0, 1, 2, 3};
  int y[4];

  skbase_array_create(&array, sizeof(int));

  skbase_array_push(&array, x + 0);
  skbase_array_push(&array, x + 1);
  skbase_array_push(&array, x + 2);
  skbase_array_push(&array, x + 3);

  SKBASE_TEST_EQlud(array.length, 4l);

  skbase_array_get_value(&array, 0, y + 0);
  skbase_array_get_value(&array, 1, y + 1);
  skbase_array_get_value(&array, 2, y + 2);
  skbase_array_get_value(&array, 3, y + 3);

  SKBASE_TEST_EQd(x[0], y[0]);
  SKBASE_TEST_EQd(x[1], y[1]);
  SKBASE_TEST_EQd(x[2], y[2]);
  SKBASE_TEST_EQd(x[3], y[3]);


  skbase_array_pop(&array);

  SKBASE_TEST_EQlud(array.length, 3l);

  skbase_array_get_value(&array, 2, y + 0);
  SKBASE_TEST_EQd(x[2], y[0]);


  skbase_array_free(&array);


  return 1;
}


int
skbase_test_array_insrem(void)
{
  skbase_array array;
  int x[4] = {0, 1, 2, 3};
  int y[4];

  skbase_array_create(&array, sizeof(int));

  skbase_array_insert(&array, 0, x + 0);
  skbase_array_insert(&array, 0, x + 1);
  skbase_array_insert(&array, 1, x + 2);
  skbase_array_insert(&array, 0, x + 3);

  SKBASE_TEST_EQlud(array.length, 4l);

  skbase_array_get_value(&array, 0, y + 0);
  skbase_array_get_value(&array, 1, y + 1);
  skbase_array_get_value(&array, 2, y + 2);
  skbase_array_get_value(&array, 3, y + 3);

  SKBASE_TEST_EQd(x[0], y[3]);
  SKBASE_TEST_EQd(x[1], y[1]);
  SKBASE_TEST_EQd(x[2], y[2]);
  SKBASE_TEST_EQd(x[3], y[0]);


  skbase_array_remove(&array, 1);
  SKBASE_TEST_EQlud(array.length, 3l);


  skbase_array_get_value(&array, 1, y + 0);
  SKBASE_TEST_EQd(x[2], y[0]);


  skbase_array_remove(&array, 0);
  SKBASE_TEST_EQlud(array.length, 2l);


  skbase_array_get_value(&array, 0, y + 0);
  skbase_array_get_value(&array, 1, y + 1);

  SKBASE_TEST_EQd(x[0], y[1]);
  SKBASE_TEST_EQd(x[2], y[0]);


  skbase_array_free(&array);


  return 1;
}


SKBASE_TESTS = {
  SKBASE_TEST_CASE(skbase_test_array_copy),
  SKBASE_TEST_CASE(skbase_test_array_pushpop),
  SKBASE_TEST_CASE(skbase_test_array_insrem),
  SKBASE_TEST_LASTCASE
};
